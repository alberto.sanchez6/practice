Feature: User login
  As a user
  I need login me in the application
  For I know the products available

  Scenario Outline: Create user
    Given the user want to create user
    When enter data in the fields
      | name           | <name>           |
      | firstLastName  | <firstLastName>  |
      | secondLastName | <secondLastName> |
      | typeId         | <typeId>         |
      | id             | <id>             |
      | email          | <email>          |
      | phone          | <phone>          |
      | password       | <password>       |
      | password2      | <password2>      |
    Then it is verified that the data is correct

    Examples:
      | name | firstLastName | secondLastName | typeId               | id       | email              | phone      | password | password2 |
      | Giss | Jhones        | Nagles         | Cédula de ciudadanía | 12345678 | salas123@gmail.com | 3140052165 | 123GLsa  | 123GLsa   |

