package models;

public class CreateUserM {

    private String name;
    private String firstLastName;
    private String secondLastName;
    private String typeId;
    private int id;
    private String email;
    private String phone;
    private String password;
    private String password2;

    public CreateUserM(String name, String firstLastName, String secondLastName, String typeId, int id, String email, String phone, String password, String password2) {
        this.name = name;
        this.firstLastName = firstLastName;
        this.secondLastName = secondLastName;
        this.typeId = typeId;
        this.id = id;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.password2 = password2;
    }

    public String getName() {
        return name;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public String getTypeId() {
        return typeId;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public String getPassword2() {
        return password2;
    }
}
