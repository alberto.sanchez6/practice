package tasks;

import cucumber.api.DataTable;
import models.CreateUserM;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import java.util.List;

import static userinterface.CreateUser.*;

public class CreateUser implements Task {


    private List<CreateUserM> datatable;

    public CreateUser(DataTable datatable) {
        this.datatable = datatable.asList(CreateUserM.class);
    }

    public static CreateUser in(DataTable datatable) {
        return Tasks.instrumented(CreateUser.class, datatable);

    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Enter.theValue(datatable.get(0).getName()).into(NAME),
                Enter.theValue(datatable.get(0).getFirstLastName()).into(FIRST_LAST_NAME),
                Enter.theValue(datatable.get(0).getSecondLastName()).into(SECOND_LAST_NAME),
                SelectFromOptions.byVisibleText(datatable.get(0).getTypeId()).from(TYPE_ID),
                //Enter.theValue(datatable.get(0).getId()).into(ID),
                Enter.theValue(datatable.get(0).getEmail()).into(EMAIL),
                Enter.theValue(datatable.get(0).getPhone()).into(PHONE),
                Enter.theValue(datatable.get(0).getPassword()).into(PASSWORD),
                Enter.theValue(datatable.get(0).getPassword2()).into(PASSWORD2),
                Click.on(CHECKBOX),
                Click.on(BTN_CREATE));


    }
}
