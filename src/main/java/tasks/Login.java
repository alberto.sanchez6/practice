package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import userinterface.LoginPage;

public class Login implements Task {


    public static Login onThePage() {
        return Tasks.instrumented(Login.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(LoginPage.START_SESION),
                Enter.theValue("salas123@gmail.com").into(LoginPage.INPUT_EMAIL),
                Enter.theValue("123GLsa").into(LoginPage.INPUT_PASSWORD),
                Click.on(LoginPage.ENTER_BUTTON2)


        );
    }


}
