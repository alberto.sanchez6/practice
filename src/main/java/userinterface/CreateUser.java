package userinterface;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

public class CreateUser extends PageObject {

    public static final Target NAME = Target.the("name").located(By.xpath("//*[@placeholder='Escribe tu nombre']"));
    public static final Target FIRST_LAST_NAME = Target.the("name").located(By.xpath("//*[@placeholder='Escribe tu primer apellido']"));
    public static final Target SECOND_LAST_NAME = Target.the("name").located(By.xpath("//*[@placeholder='Escribe tu segundo apellido']"));
    public static final Target TYPE_ID = Target.the("name").located(By.xpath("//*[@value='1']"));
    public static final Target ID = Target.the("name").located(By.xpath("//*[@type='number']"));
    public static final Target EMAIL = Target.the("name").located(By.xpath("//*[@placeholder='Escribe tu correo electrónico']"));
    public static final Target PHONE = Target.the("name").located(By.xpath("//*[@placeholder='Escribe tu teléfono de contacto']"));
    public static final Target PASSWORD = Target.the("name").located(By.xpath("//input[@placeholder='Escribe tu contraseña']"));
    public static final Target PASSWORD2 = Target.the("name").located(By.xpath("//input[@placeholder='Confirma tu contraseña']"));
    public static final Target CHECKBOX = Target.the("name").located(By.xpath("//input[@class='checkbox-input']"));
    public static final Target BTN_CREATE = Target.the("name").located(By.xpath(""));

}
