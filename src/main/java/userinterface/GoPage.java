package userinterface;


import net.thucydides.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://staging-zapatoca.miaguila.com/registro/?hidecaptcha=true")
public class GoPage extends PageObject {
}
