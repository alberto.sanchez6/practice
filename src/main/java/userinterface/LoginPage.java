package userinterface;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

public class LoginPage extends PageObject {

    public static final Target START_SESION = Target.the("option enter here").located(By.xpath("//*[@class='text-default-primary font-bold ml-2']"));
    public static final Target INPUT_EMAIL = Target.the("where do we write the email").located(By.xpath("//*[@data-role='email_textbox']"));
    public static final Target INPUT_PASSWORD = Target.the("where do we write the password").located(By.xpath("//*[@type='password']"));
    public static final Target ENTER_BUTTON2 = Target.the("button to enter login").located(By.xpath("//*[@data-role='login_button']"));

}
