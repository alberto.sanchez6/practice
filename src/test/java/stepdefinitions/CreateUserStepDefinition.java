package stepdefinitions;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import tasks.CreateUser;
import tasks.Go;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class CreateUserStepDefinition {

    @Before
    public void setStage() {
        OnStage.setTheStage(new OnlineCast());
    }

        @Given("^the user want to create user$")
    public void theUserWantToCreateUser() {
        OnStage.theActorCalled("Giss").wasAbleTo(Go.miaguilaPage());
    }

    @When("^enter data in the fields$")
    public void enterDataInTheFields(DataTable dataTable) {
        theActorInTheSpotlight().attemptsTo(CreateUser.in(dataTable));

    }

    @Then("^it is verified that the data is correct$")
    public void itIsVerifiedThatTheDataIsCorrect() {

    }



}
