package stepdefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import tasks.Go;
import tasks.Login;

public class LoginStepDefinition {


    @Before
    public void setStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^the user want to login in the page$")
    public void theUserWantToLoginInThePage() {

        OnStage.theActorCalled("Giss").wasAbleTo(Go.miaguilaPage());
    }

    @When("^the user enter value email and password$")
    public void theUserEnterValueEmailAndPassword() {
        OnStage.theActorCalled("Giss").wasAbleTo(Login.onThePage());
    }

    @Then("^it is verified that the name belongs to the user$")
    public void itIsVerifiedThatTheNameBelongsToTheUser() {

    }
}
